This contains some basic machine learning (ML) examples.

**Warning:** Do not use this code. It is an example only. There are several
known large issues with the code and the way the data is analysed. The
discussion here is of course only surface deep and more interesting questions
exist.

# Research Question

1. Can we predict a student's final grade (CGPA) based on just their first year
grades from mixed departments?

# Data

We use the open source Kaggle dataset for ["Grades of
Students"](https://www.kaggle.com/datasets/ssshayan/grades-of-students).

Description:

* `Seat No.` - The unique ID for a given student.
* `AA-BCC` - Where `AA` is the department, `B` is the year and `CC` is some
course identifier.
* `CGPA` - The student's final score.

Initial steps:

1. Download the data.
2. Decompress the data: `unzip archive.zip`.
3. Quickly check the data with: `libreoffice Grades.csv`

Now process the data with:

1. Clean the data:

```
python3 src/main.py clean data/Grades.csv data/grades_cleaned.csv
```

2. Split the data (80:20):

```
python3 src/main.py split data/grades_cleaned.csv data/grades_train.csv data/grades_test.csv 0.8
```

# Random

We train with (which in this case does nothing):

```
python3 src/main.py train random data/grades_train.csv
```

We test with:

```
python3 src/main.py test random data/grades_test.csv data/grades_train.csv
```

This gives a result approximately:

```
Correct: 0.8771929824561403 %
Error: 19.437320574162666 %
```

Which obviously makes it a very poor classifier.

As there is an element of randomness here, we would test multiple times and
take an average, perhaps plotting the standard deviation of the data to give
the reader an idea of how random it is.

# KNN

We train with (which in this case does nothing):

```
python3 src/main.py train knn data/grades_train.csv
```

We test with (where `3` is the K value):

```
python3 src/main.py test knn data/grades_test.csv data/grades_train.csv 3
```

In this case we are interesting in how the K value affects accuracy, so we run
a script to collect the data:

```
bash test_knn.sh
```

We then analyse the results (in this case using libreoffice). As you can see,
there is an optimal range for K values.

![Varying K values](doc/knn-avg.png)

# Conclusion

Regarding the original research question: "Can we predict a student's final
grade (CGPA) based on just their first year grades from mixed departments?"

We show with ~94% accuracy we can predict a student's final CGPA with just
their first year results.

In future works we would like to show whether there is predictive capability
across different academic groups and different schools.
