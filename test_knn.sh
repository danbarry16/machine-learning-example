filename="$(date).txt"
touch "$filename"
for k in {1..50}; do
  for run in {1..10}; do
    echo "k: $k" >> "$filename"
    python3 src/main.py test knn data/grades_test.csv data/grades_train.csv $k >> "$filename"
  done
done
