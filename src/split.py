import csv
import random

# split_data()
#
# Split the data into two sets.
#
# @param d_in The clean data input.
# @param d_train The output file for training.
# @param d_test The output file for testing.
# @param ratio A value between 0.0 and 1.0.
def split_data(d_in, d_train, d_test, ratio = 0.8) :
  ratio = float(ratio)
  # Open the input file
  with open(d_in, newline = "\n") as csvfile :
    # Parse the CSV file
    data = csv.reader(csvfile, delimiter = ",")
    # Calculate target lengths
    all_data = list(data)
    len_train = int(len(all_data) * ratio)
    len_test = len(all_data) - len_train
    print("Ratio: " + str(ratio) + ", train: " + str(len_train) + ", test: " + str(len_test))
    # Open the train file
    with open(d_train, "w", newline = "\n") as csvtrain :
      train = csv.writer(csvtrain, delimiter = ",", quotechar = "\"", quoting = csv.QUOTE_MINIMAL)
      # Open the test file
      with open(d_test, "w", newline = "\n") as csvtest :
        test = csv.writer(csvtest, delimiter = ",", quotechar = "\"", quoting = csv.QUOTE_MINIMAL)
        # Shuffle elements
        random.shuffle(all_data)
        # Write sets
        for i in range(len(all_data)) :
          if i < len_train :
            train.writerow(all_data.pop())
          else :
            test.writerow(all_data.pop())
  return
