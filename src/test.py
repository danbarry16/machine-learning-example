# run_test()
#
# Test the accuracy of the algorithm.
#
# @param alg The algorithm to be tested.
# @param data The data to be checked against.
def run_test(alg, data) :
  max_delta = 4.0 - (1.0 / 3.0)
  correct = 0
  error = 0.0
  total = 0
  for row in data :
    # Pass everything but the CGPA
    result = float(alg.test(row[:-1]))
    # Check whether the result is as expected
    if result == float(row[-1]) :
      correct += 1
    else :
      error += abs(result - float(row[-1])) / max_delta
    total += 1
    print(".", end = "", flush = True)
  print("")
  # NOTE: These metrics are a little wrong, but they show roughly whether
  #       algorithms are better than one another.
  print("Correct: " + str(100.0 * float(correct) / float(total)) + " %")
  print("Error: " + str(100.0 * float(error) / float(total)) + " %")
  print("Accuracy: " + str(100.0 - (100.0 * float(error) / float(total))) + " %")
  return
