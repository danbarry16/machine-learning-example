import csv

# clean_data()
#
# Parse the raw Kaggle data and export it as a cleaned data set.
#
# @param d_in Raw input data.
# @param d_out Cleaned output data.
def clean_data(d_in, d_out) :
  line_num = 0
  row_head = []
  row_len = -1
  # Open the input file
  with open(d_in, newline = "\n") as csvfile :
    # Parse the CSV file
    data = csv.reader(csvfile, delimiter = ",")
    # Open the output file
    with open(d_out, "w", newline = "\n") as csvout :
      out = csv.writer(csvout, delimiter = ",", quotechar = "\"", quoting = csv.QUOTE_MINIMAL)
      # For each line of input
      for row in data :
        line_num += 1
        # Ignore empty lines
        if len(row) < 1 :
          continue
        # Store expected length of row
        if row[0] == "Seat No." :
          print("Line " + str(line_num) + ": Header")
          row_head = row
          row_len = len(row)
          continue
        # Make sure row is of correct length
        if len(row) != row_len :
          print("Line " + str(line_num) + ": Incorrect length")
          continue
        # Limit to first year data and CGPA
        row_clean = [ ]
        for c in range(1, len(row_head)) :
          if len(row_head[c]) == 6 :
            if row_head[c][3] == '1' :
              row_clean += [ row[c] ]
        row_clean += [ row[-1] ]
        row = row_clean
        # Check for missing data
        missing = False
        for col in row :
          if len(col) <= 0 :
            missing = True
        if missing == True :
          print("Line " + str(line_num) + ": Missing data")
          continue
        # Convert data to expected range
        row_clean = []
        unclean = False
        for col in row[:-1] :
          score = -1.0
          if   col[0] == 'A' :
            score = 1.0
          elif col[0] == 'B' : 
            score = 2.0
          elif col[0] == 'C' : 
            score = 3.0
          elif col[0] == 'D' : 
            score = 4.0
          else :
            print("Line " + str(line_num) + ": Bad score letter")
            unclean = True
            continue
          # Check for a plus or minus
          if len(col) > 1 :
            if   col[1] == '-' :
              score += 1.0 / 3.0
            elif col[1] == '+' :
              score -= 1.0 / 3.0
            else :
              print("Line " + str(line_num) + ": Bad score modifier")
              unclean = True
              continue
          row_clean += [ score ]
        if unclean == True :
          continue
        row_clean += [ row[-1] ]
        row = row_clean
        # NOTE: We should also balance the dataset.
        out.writerow(row)
  return
