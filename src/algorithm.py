# algorithm.py
#
# An interface for the algorithm.
class Algorithm :
  # train()
  #
  # Train the algorithm based on the training set.
  #
  # @param data The cleaned and split CSV data to be used to train the algorithm.
  def train(self, data) :
    print("The train() function is not implemented.")
    return

  # test()
  #
  # Test the accuracy of the algorithm given a set of features.
  #
  # @return The predicted result.
  def test(self, features) :
    print("The test() function is not implemented.")
    return
