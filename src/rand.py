from algorithm import Algorithm

import csv
import random

# rand.py
#
# A random classification algorithm.
class Random(Algorithm) :
  # __init__()
  #
  #  Initialise the algorithm.
  #
  # @param args The command line arguments to initialize the algorithm.
  def __init__(self, args) :
    self.data = None
    if len(args) > 0 :
      # Open the training file if we were given one
      with open(args[0], newline = "\n") as csvfile :
        raw = csv.reader(csvfile, delimiter = ",")
        self.data = []
        for row in raw :
          self.data += [ row ]
    return

  def train(self, data) :
    # NOTE: We do nothing.
    return

  def test(self, features) :
    # Make sure we have a training data to use
    if self.data == None :
      print("No training data provided")
    # Return random CGPA from the training data
    return self.data[random.randrange(len(self.data))][-1]
