import csv
import sys

import clean
import split
import test

from knn import KNN
from rand import Random

# err()
#
# Display an error and quit the program.
#
# @param msg The message to be displayed.
def err(msg) :
  print("[!!] " + msg)
  sys.exit()
  # Never get here we hope
  return

# main()
#
# Main entry point.
#
# @param args The command line arguments.
def main(args) :
  # Check if we are running something
  if len(args) > 0 :
    if args[0] == "clean" :
      if len(args) > 2 :
        clean.clean_data(args[1], args[2])
        return
      else :
        err("Incorrect number of arguments given for clean")
    if args[0] == "help" :
      print("python3 main.py <CMD>")
      print("")
      print("  ComManD")
      print("")
      print("    clean    Clean a given dataset")
      print("               <STR> Input dataset")
      print("               <STR> Output dataset")
      print("    help     Display this help")
      print("    split    Split the data into train and test sets")
      print("               <STR> Input dataset")
      print("               <STR> Train dataset")
      print("               <STR> Test dataset")
      print("               <FLT> Split ratio")
      print("    test     Test an algorithm")
      print("               <STR> The algorithm to test")
      print("               <STR> Test dataset")
      print("               [STR] Optional algorithm parameters")
      print("    train    Train an algorithm")
      print("               <STR> The algorithm to train")
      print("               <STR> Train dataset")
      print("               [STR] Optional algorithm parameters")
      print("")
      print("  Algorithms")
      print("")
      print("    knn       K nearest neighbour.")
      print("    random    Random classification.")
      return
    if args[0] == "split" :
      if len(args) > 4 :
        split.split_data(args[1], args[2], args[3], args[4])
        return
      else :
        err("Incorrect number of arguments given for split")
    if args[0] == "test" or args[0] == "train" :
      # Make sure we have enough arguments
      if len(args) < 3 :
        err("Not enough arguments")
      # See if we can find the algorithm
      alg = None
      if args[1] == "random" :
        alg = Random(args[3:])
      elif args[1] == "knn" :
        alg = KNN(args[3:])
      else :
        err("Not enough arguments for testing/training");
      # Open the provided dataset
      with open(args[2], newline = "\n") as csvfile :
        # Parse the CSV file
        data = csv.reader(csvfile, delimiter = ",")
        # Now run the test/train
        if args[0] == "test" :
          test.run_test(alg, data)
          return
        if args[0] == "train" :
          alg.train(data)
          return
    err("Unknown command '" + args[0] + "'")
  else :
    err("No command line arguments given, see 'help'")
  return

# Enter the main program
if __name__ == "__main__" :
  main(sys.argv[1:])
