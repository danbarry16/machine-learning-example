from algorithm import Algorithm

import csv
import math
import random
import sys

# knn.py
#
# A KNN classification algorithm. This is just very basic average weighting,
# and this algorithm is super slow.
class KNN(Algorithm) :
  # __init__()
  #
  #  Initialise the algorithm.
  #
  # @param args The command line arguments to initialize the algorithm.
  def __init__(self, args) :
    self.data = None
    if len(args) > 0 :
      # Open the training file if we were given one
      with open(args[0], newline = "\n") as csvfile :
        raw = csv.reader(csvfile, delimiter = ",")
        self.data = []
        for row in raw :
          self.data += [ row ]
    # Set the K value
    self.k = -1
    if len(args) > 1 :
      self.k = int(args[1])
    return

  def train(self, data) :
    # NOTE: We do nothing.
    return

  def get_dist(self, fa, fb) :
    f_sqr_sum = 0.0
    assert(len(fa) == len(fb) - 1)
    for i in range(len(fa)) :
      f_sqr_sum += abs(float(fa[i]) - float(fb[i])) ** 2
    return math.sqrt(f_sqr_sum)

  def test(self, features) :
    # Make sure we have a training data to use
    if self.data == None :
      print("No training data provided")
    # Make sure we have a K valuee
    if self.k < 1 :
      print("No neighbours will be selected")
    # Select the K nearest neighbours
    neighbours = list(self.data)
    while len(neighbours) > self.k :
      worst = neighbours[0]
      worst_score = 0
      for point in neighbours :
        dist = self.get_dist(features, point)
        if dist > worst_score :
          worst = point
          worst_score = dist
      neighbours.remove(worst)
    # Average the neighbours
    cgpa_avg = 0
    for n in neighbours :
      cgpa_avg += float(n[-1])
    cgpa_avg /= len(neighbours)
    return cgpa_avg
